﻿using UnityEngine;
using System.Collections;

/// <summary>
/// POCO Class - Constant Data for the entire application
/// </summary>
public class Settings : MonoBehaviour
{
    public bool AutoPlay = false;
    public float StartSpeed = 5f;
    public const int LevelAmount = 15;
}
