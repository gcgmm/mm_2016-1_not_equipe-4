﻿using UnityEngine;
using System.Collections;
using System;

public static class UserPreferences
{
    #region Properties 

    public static bool HasAudio { get; set; }
    public static bool HasSong { get; set; }
    public static float AudioVolume { get; set; }
    public static float SongVolume { get; set; }
    public static DateTime LastPlayed { get; set; }
    public static bool HasPlayedBefore { get; set; }

    //Gameplay
    public static int LevelUnlocked { get; set; }

    #endregion

    public static void SavePreferences()
    {
        PlayerPrefs.SetFloat("AudioVolume", AudioVolume);
        PlayerPrefs.SetFloat("SongVolume", SongVolume);
        PlayerPrefs.SetInt("LevelUnlocked", LevelUnlocked);
        PlayerPrefs.SetString("LastPlayed", LastPlayed.ToString());
        PlayerPrefs.SetString("HasPlayedBefore", HasPlayedBefore.ToString());
    }

    //TODO: Este método é temporário... pois o level do player não deveria ser salvo junto com as preferências...
    public static void UnlockedLevel(int level)
    {
        if (LevelUnlocked < level)
        {
            LevelUnlocked = level;
            SavePreferences();
        }
    }

    public static void LoadPreferences()
    {
        //--Load all regular properties
        AudioVolume = PlayerPrefs.GetFloat("AudioVolume", 1f);
        SongVolume = PlayerPrefs.GetFloat("SongVolume", 0.8f);
        LevelUnlocked = PlayerPrefs.GetInt("LevelUnlocked", 1);
        LastPlayed = DateTime.Parse(PlayerPrefs.GetString("LastPlayed", DateTime.Now.ToString()));
        HasPlayedBefore = bool.Parse(PlayerPrefs.GetString("HasPlayedBefore", "False"));
    }
}
