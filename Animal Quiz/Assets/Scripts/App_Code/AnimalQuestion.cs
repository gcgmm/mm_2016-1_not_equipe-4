﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.App_Code
{
    public class AnimalQuestion
    {
        #region Properties

        public EAnimal AnimalId { get; private set; }
        public string AnimalName { get; private set; }

        public int QuestionId { get; set; }
        public string QuestionText { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }

        #endregion

        public AnimalQuestion(EAnimal animalId)
        {
            this.AnimalId = animalId;
            this.AnimalName = GetAnimalNameFromId(animalId);

            this.QuestionId = 0;
            this.QuestionText = string.Empty;
            this.Answer1 = string.Empty;
            this.Answer2 = string.Empty;
            this.Answer3 = string.Empty;
            this.Answer4 = string.Empty;
        }

        public static string GetAnimalNameFromId(EAnimal animalId)
        {
            switch (animalId)
            {
                case EAnimal.Galinha:
                    return "Galinha";
                case EAnimal.Vaca:
                    return "Vaca";
                case EAnimal.Porco:
                    return "Porco";
                case EAnimal.Ovelha:
                    return "Ovelha";
                case EAnimal.Bode:
                    return "Bode";
                case EAnimal.Javali:
                    return "Javali";
                case EAnimal.Coelho:
                    return "Coelho";
                case EAnimal.Lobo:
                    return "Lobo";
                case EAnimal.Pato:
                    return "Pato";
                case EAnimal.AguiaDourada:
                    return "Águia Dourada";
                case EAnimal.Coruja:
                    return "Coruja";
                case EAnimal.Pombo:
                    return "Pombo";
                case EAnimal.Gaivota:
                    return "Gaivota";
                default:
                    return string.Empty;
            }
        }

        public string GetStringFromAnimal()
        {
            return string.Format("{0}|{1}|{2}|{3}|{4}|{5}",
                    QuestionId,
                    QuestionText,
                    Answer1,
                    Answer2,
                    Answer3,
                    Answer4);
        }

        //TODO
        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static AnimalQuestion GetAnimalFromString(string text)
        {
            return null;
        }

        //TODO
        public bool ValidateString(string text)
        {
            return false;
        }

        public override string ToString()
        {
            return this.GetStringFromAnimal();
        }
    }
}
