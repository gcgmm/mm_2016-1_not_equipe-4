﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.App_Code
{
    public static class QuestionsFileHandler
    {
        public static bool WriteAllQuestions(EAnimal animalId, List<AnimalQuestion> animalQuestions)
        {
            string path = string.Format("{0}/{1}.txt",
                            Application.persistentDataPath,
                            ((int)animalId).ToString());

            //Delete file if already exists
            if (File.Exists(path)) File.Delete(path);

            StringBuilder sb = new StringBuilder(animalQuestions.Count);
            foreach (var item in animalQuestions)
            {
                sb.AppendLine(string.Format("{0}|{1}|{2}|{3}|{4}|{5}",
                    item.QuestionId,
                    item.QuestionText,
                    item.Answer1,
                    item.Answer2,
                    item.Answer3,
                    item.Answer4));
            }

            try
            {
                File.WriteAllText(path, sb.ToString());
            }
            catch (Exception ex)
            {
                Debug.Log("Erro ao tentar salvar perguntas: " + ex.Message);

                return false;
            }

            return true;
        }


        /// <summary>
        /// Read all questions from ALL Animals.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<EAnimal, List<AnimalQuestion>> ReadAllQuestions()
        {
            Dictionary<EAnimal, List<AnimalQuestion>> allQuestions = new Dictionary<EAnimal, List<AnimalQuestion>>();
            for (int i = 1; i <= 15; i++)
            {
                var questions = ReadAllQuestions((EAnimal)i);

                if (questions != null)
                    allQuestions.Add((EAnimal)i, questions);
            }
            
            return allQuestions;
        }


        /// <summary>
        /// Read all questions from ONE GIVEN Animal specified in the parameter.
        /// Will return NULL if the animal file does not exists!
        /// </summary>
        /// <param name="animalId"></param>
        /// <returns></returns>
        public static List<AnimalQuestion> ReadAllQuestions(EAnimal animalId)
        {
            string path = string.Format("{0}/{1}.txt",
                                        Application.persistentDataPath,
                                        ((int)animalId).ToString());

            if (File.Exists(path))
            {
                List<AnimalQuestion> animalQuestions = new List<AnimalQuestion>(3);
                string[] perguntas = File.ReadAllLines(path);

                try
                {
                    for (int i = 0; i < perguntas.Length; i++)
                    {
                        string[] conteudoPergunta = perguntas[i].Split(new char[] { '|' }, System.StringSplitOptions.RemoveEmptyEntries);
                        if (conteudoPergunta.Length >= 5)
                        {
                            AnimalQuestion question = new AnimalQuestion(animalId)
                            {
                                QuestionId = int.Parse(conteudoPergunta[0]),
                                QuestionText = conteudoPergunta[1],
                                Answer1 = conteudoPergunta[2],
                                Answer2 = conteudoPergunta[3],
                                Answer3 = conteudoPergunta[4],
                                Answer4 = (conteudoPergunta.Length >= 5) ? conteudoPergunta[5] : string.Empty
                            };

                            animalQuestions.Add(question);
                        }
                        else
                        {
                            Debug.Log("Pulado linha em branco no arquivo " + path);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.Log("Erro ao tentar ler perguntas/respostas: " + ex.Message);

                    throw;
                }

                return animalQuestions;
            }
            else
            {
                Debug.Log(string.Format("Arquivo '{0}' não existe.", path));

                return null;
            }
        }

        public static string GetAnimalFilePath(EAnimal animalId)
        {
            return string.Format("{0}/{1}.txt",
                            Application.persistentDataPath,
                            ((int)animalId).ToString());
        }

        /// <summary>
        /// CAREFUL!!!!!!! All current saved questions will be erased and the default ones will be setted.
        /// </summary>
        public static void ResetAllAnimalsQuestions()
        {
            Dictionary<EAnimal, List<AnimalQuestion>> allAnimalsQuestions = new Dictionary<EAnimal, List<AnimalQuestion>>(15);

            //Animal 1 (Galinha)
            var questionsAnimal1 = new List<AnimalQuestion>(3);
            questionsAnimal1.Add(new AnimalQuestion(EAnimal.Galinha)
            {
                QuestionId = 1,
                QuestionText = "A galinha é um animal:",
                Answer1 = "Onívoro ",
                Answer2 = "Herbívoro ",
                Answer3 = "Carnívoro ",
                Answer4 = "Biterivoro"
            });
            questionsAnimal1.Add(new AnimalQuestion(EAnimal.Galinha)
            {
                QuestionId = 2,
                QuestionText = "Quais características fazem da galinha uma ave:",
                Answer1 = "Penas, bico e asas",
                Answer2 = "Crista, boca e patas",
                Answer3 = "Cabeça, olhos e botar ovos",
                Answer4 = "Penas, poder voar e ser carnívoro"
            });
            questionsAnimal1.Add(new AnimalQuestion(EAnimal.Galinha)
            {
                QuestionId = 3,
                QuestionText = "A galinha come vísceras de animais, insetos e milho, é um animal:",
                Answer1 = "Onívoro",
                Answer2 = "Herbívoro",
                Answer3 = "Vertífero",
                Answer4 = "Carbíforo"
            });
            allAnimalsQuestions.Add(EAnimal.Galinha, questionsAnimal1);

            //Animal 2 (Vaca)
            var questionsAnimal2 = new List<AnimalQuestion>(3);
            questionsAnimal2.Add(new AnimalQuestion(EAnimal.Vaca)
            {
                QuestionId = 1,
                QuestionText = "A vaca amamenta seu bezerro, sendo assim ela é um animal:",
                Answer1 = "Mamífero",
                Answer2 = "Vertebrado",
                Answer3 = "Naníforo",
                Answer4 = "Herbíforo"
            });
            questionsAnimal2.Add(new AnimalQuestion(EAnimal.Vaca)
            {
                QuestionId = 2,
                QuestionText = "A vaca é um animal herbívoro, ela come:",
                Answer1 = "Grama, pasto e plantas",
                Answer2 = "Insetos, aves e feno",
                Answer3 = "Carne bovina e suína",
                Answer4 = "Pasto, ovos e adubo"
            });
            questionsAnimal2.Add(new AnimalQuestion(EAnimal.Vaca)
            {
                QuestionId = 3,
                QuestionText = "A alimentação da vaca é classificada como:",
                Answer1 = "Herbívora",
                Answer2 = "Ovivora",
                Answer3 = "Erbívora",
                Answer4 = "Carnívora"
            });
            allAnimalsQuestions.Add(EAnimal.Vaca, questionsAnimal2);

            //Animal 3 (Porco)
            var questionsAnimal3 = new List<AnimalQuestion>(3);
            questionsAnimal3.Add(new AnimalQuestion(EAnimal.Porco)
            {
                QuestionId = 1,
                QuestionText = "O porco é um mamífero, por quê:",
                Answer1 = "Tem glândulas mamárias",
                Answer2 = "É um suíno",
                Answer3 = "Alimenta de vegetais",
                Answer4 = "Tem quatro patas e penas"
            });
            questionsAnimal3.Add(new AnimalQuestion(EAnimal.Porco)
            {
                QuestionId = 2,
                QuestionText = "A alimentação do porco é feita por aveia, milho, cevada e trigo, ele é um animal:",
                Answer1 = "Herbívoro",
                Answer2 = "Carnívoro",
                Answer3 = "Ovíparo",
                Answer4 = "Onívoro"
            });
            questionsAnimal3.Add(new AnimalQuestion(EAnimal.Porco)
            {
                QuestionId = 3,
                QuestionText = "O porco se desenvolve no interior do corpo da mãe, então ele é um animal:",
                Answer1 = "Mamífero",
                Answer2 = "Carnívoro",
                Answer3 = "Ovíparo",
                Answer4 = "Herbívoro"
            });
            allAnimalsQuestions.Add(EAnimal.Porco, questionsAnimal3);

            //Animal 4 (Ovelha)
            var questionsAnimal4 = new List<AnimalQuestion>(3);
            questionsAnimal4.Add(new AnimalQuestion(EAnimal.Ovelha)
            {
                QuestionId = 1,
                QuestionText = "A ovelha é um animal: (classe animal)",
                Answer1 = "Mamífero",
                Answer2 = "Ovíparo",
                Answer3 = "Vertebrado",
                Answer4 = "Carnívoro"
            });
            questionsAnimal4.Add(new AnimalQuestion(EAnimal.Ovelha)
            {
                QuestionId = 2,
                QuestionText = "A ovelha é um mamífero quadrúpede e:",
                Answer1 = "Herbívoro",
                Answer2 = "Ovíparo",
                Answer3 = "Vegetal",
                Answer4 = "Carnívoro"
            });
            questionsAnimal4.Add(new AnimalQuestion(EAnimal.Ovelha)
            {
                QuestionId = 3,
                QuestionText = "Quais as características fazem da ovelha um mamífero?",
                Answer1 = "Fêmeas tem glândulas mamárias",
                Answer2 = "Têm ossos e coluna vertebral",
                Answer3 = "Possuem assas, bicos e pelos",
                Answer4 = "Têm patas e sabem nadar"
            });
            allAnimalsQuestions.Add(EAnimal.Ovelha, questionsAnimal4);

            //Animal 5 (Bode)
            var questionsAnimal5 = new List<AnimalQuestion>(3);
            questionsAnimal5.Add(new AnimalQuestion(EAnimal.Bode)
            {
                QuestionId = 1,
                QuestionText = "Qual a alimentação do bode?",
                Answer1 = "Herbívoro",
                Answer2 = "Biveterino",
                Answer3 = "Oxivoro",
                Answer4 = "Ninívoro"
            });
            questionsAnimal5.Add(new AnimalQuestion(EAnimal.Bode)
            {
                QuestionId = 2,
                QuestionText = "Qual classe animal o bode pertence ?",
                Answer1 = "Mamífero",
                Answer2 = "Mamilos",
                Answer3 = "Ovíparo",
                Answer4 = "Vertebrado"
            });
            questionsAnimal5.Add(new AnimalQuestion(EAnimal.Bode)
            {
                QuestionId = 3,
                QuestionText = "O bode é um animal herbívoro por se alimentar de?",
                Answer1 = "Grama, arbustos, pasto",
                Answer2 = "Arbusto, carne e aves",
                Answer3 = "Grama, pasto e suínos",
                Answer4 = "Pasto, feno e peixe"
            });
            allAnimalsQuestions.Add(EAnimal.Bode, questionsAnimal5);

            //Animal 6 (Javali)
            var questionsAnimal6 = new List<AnimalQuestion>(3);
            questionsAnimal6.Add(new AnimalQuestion(EAnimal.Javali)
            {
                QuestionId = 1,
                QuestionText = "O Javali é um animal selvagem classificado como:",
                Answer1 = "Mamífero e onívoro",
                Answer2 = "Invertebrado e mamífelo",
                Answer3 = "Quadrupede e náutico",
                Answer4 = "Terrestre e ovíparo"
            });
            questionsAnimal6.Add(new AnimalQuestion(EAnimal.Javali)
            {
                QuestionId = 2,
                QuestionText = "Em qual classe animal o javali pertence?",
                Answer1 = "Mamífero",
                Answer2 = "Mamilos",
                Answer3 = "Ovíparo",
                Answer4 = "Vertebrado"
            });
            questionsAnimal6.Add(new AnimalQuestion(EAnimal.Javali)
            {
                QuestionId = 3,
                QuestionText = "O Javali se alimenta com vegetais e carne de pequenos animais então ele é:",
                Answer1 = "Onívoros",
                Answer2 = "Herbívoro",
                Answer3 = "Vegetariano",
                Answer4 = "Carnívoro"
            });
            allAnimalsQuestions.Add(EAnimal.Javali, questionsAnimal6);

            //Animal 7 (Coelho)
            var questionsAnimal7 = new List<AnimalQuestion>(3);
            questionsAnimal7.Add(new AnimalQuestion(EAnimal.Coelho)
            {
                QuestionId = 1,
                QuestionText = "O coelho possui glândulas mamarias então ele é?",
                Answer1 = "Mamífero",
                Answer2 = "Vertebrado",
                Answer3 = "Ovíparo",
                Answer4 = "Mamilo"
            });
            questionsAnimal7.Add(new AnimalQuestion(EAnimal.Coelho)
            {
                QuestionId = 2,
                QuestionText = "A alimentação do coelho é de vegetais e ração, ele é um animal?",
                Answer1 = "Herbívoro",
                Answer2 = "Carnívoro",
                Answer3 = "Onívoro",
                Answer4 = "Biterívoro"
            });
            questionsAnimal7.Add(new AnimalQuestion(EAnimal.Coelho)
            {
                QuestionId = 3,
                QuestionText = "Em qual classe animal o coelho pertence?",
                Answer1 = "Mamífero",
                Answer2 = "Mamilos",
                Answer3 = "Ovíparo",
                Answer4 = "Vertebrado"
            });
            allAnimalsQuestions.Add(EAnimal.Coelho, questionsAnimal7);

            //Animal 8 (Lobo)
            var questionsAnimal8 = new List<AnimalQuestion>(3);
            questionsAnimal8.Add(new AnimalQuestion(EAnimal.Lobo)
            {
                QuestionId = 1,
                QuestionText = "O lobo é um animal selvagem mamífero que possui?",
                Answer1 = "Pelos, caninos e 4 patas",
                Answer2 = "Cabelos e pelos",
                Answer3 = "4 patas e dentes pequenos",
                Answer4 = "Olhos, bico e patas"
            });
            questionsAnimal8.Add(new AnimalQuestion(EAnimal.Lobo)
            {
                QuestionId = 2,
                QuestionText = "Em qual classe animal o lobo pertence?",
                Answer1 = "Mamífero ",
                Answer2 = "Ovíparo",
                Answer3 = "Vertebrado",
                Answer4 = "Carnívoro"
            });
            questionsAnimal8.Add(new AnimalQuestion(EAnimal.Lobo)
            {
                QuestionId = 3,
                QuestionText = "A alimentação do lobo é classificada como?",
                Answer1 = "Carnívora ",
                Answer2 = "Vegetariana",
                Answer3 = "Ovípara",
                Answer4 = "Carnivuta"
            });
            allAnimalsQuestions.Add(EAnimal.Lobo, questionsAnimal8);

            //Animal 9 (Pato)
            var questionsAnimal9 = new List<AnimalQuestion>(3);
            questionsAnimal9.Add(new AnimalQuestion(EAnimal.Pato)
            {
                QuestionId = 1,
                QuestionText = "Qual desses animais é uma ave:",
                Answer1 = "Pato",
                Answer2 = "Mosca",
                Answer3 = "Ovelha ",
                Answer4 = "Borboleta"
            });
            questionsAnimal9.Add(new AnimalQuestion(EAnimal.Pato)
            {
                QuestionId = 2,
                QuestionText = "Minha alimentação pertence a qual classe:",
                Answer1 = "Onívoro ",
                Answer2 = "Mamífero",
                Answer3 = "Herbívoro",
                Answer4 = "Carnívoro "
            });
            questionsAnimal9.Add(new AnimalQuestion(EAnimal.Pato)
            {
                QuestionId = 3,
                QuestionText = "O animal onívoro se alimenta com:",
                Answer1 = "Vegetais e carne de animais",
                Answer2 = "Somente de vegetais",
                Answer3 = "Se alimenta de carne",
                Answer4 = "Nenhuma está correta"
            });
            allAnimalsQuestions.Add(EAnimal.Pato, questionsAnimal9);

            //Animal 10 (Aguia)
            var questionsAnimal10 = new List<AnimalQuestion>(3);
            questionsAnimal10.Add(new AnimalQuestion(EAnimal.AguiaDourada)
            {
                QuestionId = 1,
                QuestionText = "Por me alimentar de carne, sou classificada como:",
                Answer1 = "Carnívora",
                Answer2 = "Mamífera",
                Answer3 = "Onívora",
                Answer4 = "Herbívora"
            });
            questionsAnimal10.Add(new AnimalQuestion(EAnimal.AguiaDourada)
            {
                QuestionId = 2,
                QuestionText = "Possuo asas, penas e bicos, sou uma:",
                Answer1 = "Ave",
                Answer2 = "Aracnídea ",
                Answer3 = "Marinha ",
                Answer4 = "Pata"
            });
            allAnimalsQuestions.Add(EAnimal.AguiaDourada, questionsAnimal10);

            //Animal 11 (Coruja)
            var questionsAnimal11 = new List<AnimalQuestion>(3);
            questionsAnimal11.Add(new AnimalQuestion(EAnimal.Coruja)
            {
                QuestionId = 1,
                QuestionText = "A coruja se classifica como:",
                Answer1 = "Carnívora",
                Answer2 = "Réptil",
                Answer3 = "Mamífera",
                Answer4 = "Humana"
            });
            questionsAnimal11.Add(new AnimalQuestion(EAnimal.Coruja)
            {
                QuestionId = 2,
                QuestionText = "A coruja é uma ave por possuir:",
                Answer1 = "Bico, penas e asas",
                Answer2 = "Asas, bico e patas",
                Answer3 = "Penas, bico e pelos",
                Answer4 = "Asas, patas e pelos"
            });            
            allAnimalsQuestions.Add(EAnimal.Coruja, questionsAnimal11);

            //Animal 12 (Pombo)
            var questionsAnimal12 = new List<AnimalQuestion>(3);
            questionsAnimal12.Add(new AnimalQuestion(EAnimal.Pombo)
            {
                QuestionId = 1,
                QuestionText = "O pombo da cidade voa, tem bico e pelos, ele é um animal classificado como?",
                Answer1 = "Ave",
                Answer2 = "Quadrupede",
                Answer3 = "Onívoro",
                Answer4 = "Mamífero"
            });
            questionsAnimal12.Add(new AnimalQuestion(EAnimal.Pombo)
            {
                QuestionId = 2,
                QuestionText = "O pombo é um animal que pertencente à classe dos?",
                Answer1 = "Onívoros",
                Answer2 = "Carnívoros",
                Answer3 = "Quadrúpedes",
                Answer4 = "Mamíferos "
            });
            questionsAnimal12.Add(new AnimalQuestion(EAnimal.Pombo)
            {
                QuestionId = 3,
                QuestionText = "O pombo se alimenta de plantas, insetos e outros animais, ele é?",
                Answer1 = "Onívoro",
                Answer2 = "Herbívoro",
                Answer3 = "Carnívoro",
                Answer4 = "Vegetariano"
            });
            allAnimalsQuestions.Add(EAnimal.Pombo, questionsAnimal12);

            //Animal 13 (Gaivota)
            var questionsAnimal13 = new List<AnimalQuestion>(3);
            questionsAnimal13.Add(new AnimalQuestion(EAnimal.Gaivota)
            {
                QuestionId = 1,
                QuestionText = "A gaivota voa, tem bico e pelos, ele é um animal classificado como?",
                Answer1 = "Ave",
                Answer2 = "Quadrupede",
                Answer3 = "Onívoro",
                Answer4 = "Mamífero"
            });
            questionsAnimal13.Add(new AnimalQuestion(EAnimal.Gaivota)
            {
                QuestionId = 2,
                QuestionText = "Por se alimentar da carne dos animais, sou um animal?",
                Answer1 = "Carnívoro",
                Answer2 = "Herbívoro",
                Answer3 = "Réptil ",
                Answer4 = "Ave"
            });
            questionsAnimal13.Add(new AnimalQuestion(EAnimal.Gaivota)
            {
                QuestionId = 3,
                QuestionText = "A gaivota é uma ave por?",
                Answer1 = "Ter asas, bico e poder voar",
                Answer2 = "Voar, ter asas e botar ovos",
                Answer3 = "Ser herbívoro e voar",
                Answer4 = "Ter patas e asas"
            });
            allAnimalsQuestions.Add(EAnimal.Gaivota, questionsAnimal13);

            //Write all questions (it will overwrite if already exists)
            foreach (var item in allAnimalsQuestions)
                WriteAllQuestions(item.Key, item.Value);
        }
    }
}
