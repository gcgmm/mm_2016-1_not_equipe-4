﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections;
using UnityEngine;

namespace Assets
{
    public static class AudioFade
    {
        public static bool IsRunning = false;
        private static bool stop = false;

        public static IEnumerator Crossfade(AudioSource a1, AudioSource a2, float duration)
        {
            IsRunning = true;

            if ((a1 != null || a2 != null) && a1 != a2)
            {
                float startA1 = 0f;
                float startA2 = 0f;

                if (a1 != null)
                    startA1 = a1.volume;

                if (a2 != null)
                {
                    startA2 = a2.volume;
                    a2.volume = 0f;
                    a2.Play();
                }

                var startTime = Time.time;
                var endTime = startTime + duration;

                while (Time.time < endTime)
                {
                    if (stop)   //If somebody has asked us to STOP
                    {
                        //Make sure everything is top notch AND STOPPED.
                        if (a1 != null)
                        {
                            a1.Stop();
                            a1.volume = startA1;
                        }
                        if (a2 != null)
                        {
                            a2.Stop();
                            a2.volume = startA2;
                        }

                        //Let's reset our control variables
                        IsRunning = false;
                        stop = false;

                        //Exit the function once for all
                        yield break;
                    }

                    var volA1 = (Time.time - startTime) / duration * startA1;
                    var volA2 = (Time.time - startTime) / duration * startA2;
                    if (a1 != null)
                        a1.volume = (startA1 - volA1);
                    if (a2 != null)
                        a2.volume = volA2;

                    yield return null;
                }

                //Make sure everything is top notch :)
                if (a1 != null)
                {
                    a1.Stop();
                    a1.volume = startA1;
                }
                if (a2 != null)
                    a2.volume = startA2;

                if (a1 != null)
                    Debug.LogFormat("A1 {0} - {1}", a1.name, a1.volume);
                if (a2 != null)
                    Debug.LogFormat("A2 {0} - {1}", a2.name, a2.volume);
            }


            IsRunning = false;
        }

        public static void StopCrossfade()
        {
            stop = true;
        }

        public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
        {
            float startVolume = audioSource.volume;

            while (audioSource.volume > 0)
            {
                //Debug.Log("Entrei Fadeout - : " + audioSource.volume);
                audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

                yield return null;
            }

            audioSource.Stop();
            audioSource.volume = startVolume;
            Debug.LogWarning("Fade Out VOLUME after: " + startVolume);
        }

        public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime)
        {
            float endVolume = audioSource.volume;
            audioSource.volume = 0.1f;
            audioSource.Play();

            while (audioSource.volume < endVolume)
            {
                Debug.Log("Entrei Fadein - : " + audioSource.volume);
                audioSource.volume += 1f * Time.deltaTime / FadeTime;
                if (audioSource.volume > 0.99f)
                    break;

                yield return null;
            }

            audioSource.volume = endVolume;
        }

    }
}
