﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Assets.Scripts.App_Code;
using UnityEngine.UI;

public class QuestionsManagerController : MonoBehaviour
{
    private EAnimal animalBeingEdited = EAnimal.None;
     
    public Text actionText;
    public InputField questionsText;
    public GameObject chooseAnimalPanel;
    public GameObject editAnimalPanel;
    public GameObject resetQuestionsPanel;

    // Use this for initialization
    void Start()
    {
        //Make sure everything is not active... the controller will take care of which ones should be visible and which ones shoudln't.
        chooseAnimalPanel.SetActive(false);
        editAnimalPanel.SetActive(false);
        resetQuestionsPanel.SetActive(false);

        DisplayChoosePanel();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GoTo_MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ChooseAnimal(int animalId)
    {
        //
        animalBeingEdited = (EAnimal)animalId;

        //
        DisplayEditPanel();

        //
        var questions = QuestionsFileHandler.ReadAllQuestions(animalBeingEdited);
        if (questions != null)  //If has questions and file does exists
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder(questions.Count);
            foreach (var item in questions)
                sb.AppendLine(item.ToString());

            questionsText.text = sb.ToString();
        }
        else    //This animal doesn't have a file or any questions yet
        {
            questionsText.text = string.Empty;
        }        
    }

    public void SaveCurrentAnimal()
    {
        //QuestionsFileHandler.WriteAllQuestions(animalBeingEdited, );
        System.IO.File.WriteAllText(QuestionsFileHandler.GetAnimalFilePath(animalBeingEdited), questionsText.text);

        DisplayChoosePanel();            
    }

    public void CancelCurrentAnimal()
    {
        DisplayChoosePanel();
    }

    public void DisplayResetAllAnimalsConfirmation(bool show)
    {
        resetQuestionsPanel.SetActive(show);
    }    

    public void ResetAllAnimalsQuestions()
    {
        QuestionsFileHandler.ResetAllAnimalsQuestions();
    }

    public void ResetAnimalQuestions(int animalId)
    {

    }

    public void AddDelimiterInTextInput()
    {
        questionsText.text += "|";
    }

    private void DisplayChoosePanel()
    {
        animalBeingEdited = EAnimal.None;

        editAnimalPanel.SetActive(false);
        chooseAnimalPanel.SetActive(true);
        actionText.text = "Escolha um dos animais para editar as perguntas";
    }

    private void DisplayEditPanel()
    {
        chooseAnimalPanel.SetActive(false);
        editAnimalPanel.SetActive(true);

        string animalName = AnimalQuestion.GetAnimalNameFromId(animalBeingEdited);
        string prefixGender = (animalName.ToLower().EndsWith("a")) ? "da" : "do";
        actionText.text = string.Format("Edite as perguntas {2} {0} ({1})",
                                        animalName,
                                        (int)animalBeingEdited,
                                        prefixGender);
    }
}
