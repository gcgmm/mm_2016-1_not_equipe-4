﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.App_Code
{
    public enum EAnimal
    {
        None = 0,
        Galinha = 1,
        Vaca,
        Porco,
        Ovelha,
        Bode,
        Javali,
        Coelho,
        Lobo,
        Pato,
        AguiaDourada,
        Coruja,
        Pombo,
        Gaivota
    }
}
