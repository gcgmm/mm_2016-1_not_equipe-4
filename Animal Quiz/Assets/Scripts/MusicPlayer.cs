﻿using Assets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class MusicPlayer : MonoBehaviour
    {
        public AudioSource mainSong;
        public AudioSource backgroundSound;

        public static MusicPlayer instance = null;
        public AudioSource currentSong;
        private IEnumerator currentCoroutine;

        void Awake()
        {
            if (instance != null)
            {
                //Self destruct duplicate music
                Destroy(gameObject);
                print("Destroyed music");
            }
            else
            {
                instance = this;
                GameObject.DontDestroyOnLoad(gameObject);
                print("created music");
            }
        }

        public void PlayMainSong()
        {
            ChangeAudio(mainSong, 2f);
        }

        public void PlayBackgroundSound()
        {
            ChangeAudio(backgroundSound, 2f);
        }

        public void StopAll()
        {
            if (mainSong.isPlaying)
                mainSong.Stop();

            if (backgroundSound.isPlaying)
                backgroundSound.Stop();

            currentSong = null;
        }

        public void ChangeAudio(AudioSource newAudio, float transitionTime)
        {
            ChangeAudio(currentSong, newAudio, transitionTime);
        }

        public void ChangeAudio(AudioSource oldAudio, AudioSource newAudio, float transitionTime)
        {
            //There is no audio fade (currentCoroutine) being executed, so we're all good
            if (currentCoroutine == null)
            {
                //The current coroutine will be this one
                currentCoroutine = AudioFade.Crossfade(oldAudio, newAudio, transitionTime);

                //And we simply start regular ChangeCrossFade
                StartCoroutine(ChangeCrossfade());
            }
            //In this case there is a cross fade in action at the moment!!
            else
            {
                //Let's say that the old audio is NULL (just cut it) and then fade in though the new audio.
                StartCoroutine(ChangeCrossfade(AudioFade.Crossfade(null, newAudio, transitionTime)));
            }

            currentSong = newAudio;
        }

        IEnumerator ChangeCrossfade(IEnumerator swapCoroutine = null)
        {
            //If there is a routine being executed and we are supposed to swap it! (Stop it and change to a new coroutine)
            if (swapCoroutine != null)
            {
                //Let's cancel the currentFade
                AudioFade.StopCrossfade();

                //While it is still being executed we wait!
                while (currentCoroutine != null) yield return null;

                //Now we set the current courtine as being the one passed as argument
                currentCoroutine = swapCoroutine;
            }

            //Simply execute the audiofade accordingly to the currentCoroutine.
            yield return StartCoroutine(currentCoroutine);

            //After coroutine has been completed we set currentCourtine to null as there is no action being executed.
            currentCoroutine = null;
        }
    }
}