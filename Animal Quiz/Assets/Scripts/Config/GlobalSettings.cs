﻿using UnityEngine;
using System.Collections;

public class GlobalSettings : Settings
{

    private static GlobalSettings s_Instance = null;

    // This defines a static instance property that attempts to find the manager object in the scene and
    // returns it to the caller.
    public static GlobalSettings instance
    {
        get
        {
            if (s_Instance == null)
            {
                // This is where the magic happens.
                //  FindObjectOfType(...) returns the first AManager object in the scene.
                s_Instance = FindObjectOfType(typeof(GlobalSettings)) as GlobalSettings;
            }

            // If it is still null, create a new instance
            if (s_Instance == null)
            {
                GameObject obj = new GameObject("GlobalSettings");
                s_Instance = obj.AddComponent(typeof(GlobalSettings)) as GlobalSettings;
                Debug.Log("Could not locate an GlobalSettings object. GlobalSettings was Generated Automaticly.");
            }

            return s_Instance;
        }
    }

}
