﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.App_Code
{
    public class UserScore
    {
        #region Properties

        public string Username { get; private set; }

        public EAnimal AnimalId { get; set; }
        public int QuestionId { get; set; }
        public int CorrectAnswers { get; set; }
        public int WrongAnswers { get; set; }

        #endregion

        public UserScore(string username)
        {
            this.Username = username;

            this.AnimalId = EAnimal.None;
            this.QuestionId = 0;
            this.CorrectAnswers = 0;
            this.WrongAnswers = 0;
            this.QuestionId = 0;
        }

        public string GetFormattedStringFromUserScore(string questionText = null)
        {
            return string.Format("{0} - Pergunta {1}\t Acertos: {2}\t Erros: {3}",
                    AnimalQuestion.GetAnimalNameFromId(AnimalId),
                    (questionText != null) ? questionText : QuestionId.ToString(),
                    CorrectAnswers,
                    WrongAnswers);
        }

        public string GetStringFromUserScore()
        {
            return string.Format("{0}|{1}|{2}|{3}",
                    (int)AnimalId,
                    QuestionId,
                    CorrectAnswers,
                    WrongAnswers);
        }

        public override string ToString()
        {
            return this.GetStringFromUserScore();
        }
    }
}