﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Assets.Scripts.App_Code;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;
using Assets;

public class MainMenuController : MonoBehaviour
{
    public Dropdown dropdownUsers;
    public InputField userScoreInput;
    public InputField usernameInput;
    public Text errorText;
    public GameObject loadingPanel;
    public RawImage loadingSpinner;

    private Dictionary<string, List<UserScore>> dcScores = new Dictionary<string, List<UserScore>>();
    private Dictionary<EAnimal, List<AnimalQuestion>> dcQuestions = new Dictionary<EAnimal, List<AnimalQuestion>>();

    // Use this for initialization
    void Start()
    {
        loadingPanel.SetActive(false);
        MusicPlayer.instance.ChangeAudio(MusicPlayer.instance.mainSong, 2f);

        //Load all user data
        UserPreferences.LoadPreferences();

        //Load all questions
        dcQuestions = QuestionsFileHandler.ReadAllQuestions();

        //If user has never played before
        if (!UserPreferences.HasPlayedBefore)
        {
            ScoreFileHandler.ResetAllUsersScore();
            QuestionsFileHandler.ResetAllAnimalsQuestions();
            UserPreferences.HasPlayedBefore = true;
            UserPreferences.SavePreferences();
        }

        //Load all scores
        dcScores = ScoreFileHandler.ReadAllScores();
        LoadDropDownUsers();

        if (!string.IsNullOrEmpty(GlobalData.CurrentUsername))
            usernameInput.text = GlobalData.CurrentUsername;
    }

    void Update()
    {
        if (loadingPanel.activeSelf)
            loadingSpinner.rectTransform.Rotate(Vector3.forward, 90.0f * Time.deltaTime);
    }

    private void LoadDropDownUsers()
    {
        if (dcScores.Count > 0)
        {
            userScoreInput.text = string.Empty;
            var lsUsers = new List<string>(dcScores.Count);
            foreach (var user in dcScores)
                lsUsers.Add(user.Key);

            lsUsers.Sort();

            foreach (var user in lsUsers)
                dropdownUsers.options.Add(new Dropdown.OptionData(user));

            dropdownUsers.captionText.text = "Toque aqui para exibir os alunos";
            dropdownUsers.value = 0;    //Força para selecionar o 1° aluno.
        }
        else
        {
            //dropdownUsers.gameObject.SetActive(false);
            userScoreInput.text = "Não há nenhum recorde gravado para escolher e/ou exibir.";
            dropdownUsers.captionText.text = "Nenhum aluno para exibir";
        }

        dropdownUsers.interactable = dcScores.Count > 0;
    }

    public void dropdownUsers_OnValueChanged(Dropdown target)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var item in dcScores[target.options[target.value].text])
        {
            var animalQuestion = dcQuestions[item.AnimalId].Find(p => p.QuestionId == item.QuestionId);

            //A questão pode ter sido deletada! Neste caso o texto da questão será null pois não temos mais acesso... 
            //e será exibido somente o ID da questão.
            string questionText = (animalQuestion != null) ? animalQuestion.QuestionText : null;

            //sb.AppendLine(item.GetFormattedStringFromUserScore(questionText));
            sb.AppendLine(item.GetFormattedStringFromUserScore());
        }

        userScoreInput.text = sb.ToString();
    }

    public void GoTo_Play()
    {
        int temp;
        if (!string.IsNullOrEmpty(usernameInput.text) && usernameInput.text.Length >= 3 && !int.TryParse(usernameInput.text, out temp))
        {
            errorText.text = string.Empty;

            //
            GlobalData.CurrentUsername = usernameInput.text;

            //
            loadingPanel.SetActive(true);

            //
            SceneManager.LoadSceneAsync("Game");

            //
            MusicPlayer.instance.ChangeAudio(MusicPlayer.instance.backgroundSound, 5f);
            //MusicPlayer.instance.ChangeAudio(null, 2f);
        }
        else
        {
            errorText.text = "O seu nome deve ter pelo menos 3 caracteres, e não pode ter somente números.";
        }
    }

    public void GoTo_ManageQuestions()
    {
        loadingPanel.SetActive(true);
        SceneManager.LoadSceneAsync("QuestionsManager");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
