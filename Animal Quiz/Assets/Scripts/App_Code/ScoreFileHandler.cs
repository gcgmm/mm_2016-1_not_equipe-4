﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using Assets.Scripts.App_Code;
using System.Collections.Generic;
using System.Text;

public static class ScoreFileHandler
{
    public static bool WriteAllScores(string username, List<UserScore> userScores)
    {
        string path = string.Format("{0}/{1}.txt",
                        Application.persistentDataPath,
                        username);

        //Delete file if already exists
        if (File.Exists(path)) File.Delete(path);

        StringBuilder sb = new StringBuilder(userScores.Count);
        foreach (var item in userScores)
        {
            sb.AppendLine(string.Format("{0}|{1}|{2}|{3}",
                (int)item.AnimalId,
                item.QuestionId,
                item.CorrectAnswers,
                item.WrongAnswers));
        }

        try
        {
            File.WriteAllText(path, sb.ToString());
        }
        catch (Exception ex)
        {
            Debug.Log("Erro ao tentar salvar scores: " + ex.Message);

            return false;
        }

        return true;
    }


    /// <summary>
    /// Read all scores from ALL Usernames.
    /// </summary>
    /// <returns></returns>
    public static Dictionary<string, List<UserScore>> ReadAllScores()
    {
        var allScores = new Dictionary<string, List<UserScore>>();

        var files = Directory.GetFiles(Application.persistentDataPath);
        foreach (var filePath in files)
        {
            var fileNameOnly = Path.GetFileNameWithoutExtension(filePath);
            int temp = 0;

            //Get all files that does not start with a number (animals will start with a number, users will be the others)
            if (!int.TryParse(fileNameOnly, out temp))
                allScores.Add(fileNameOnly, ReadAllScores(fileNameOnly));
        }

        return allScores;
    }


    /// <summary>
    /// Read all scores from ONE GIVEN Username specified in the parameter.
    /// Will return NULL if the username file does not exists!
    /// </summary>
    /// <param name="animalId"></param>
    /// <returns></returns>
    public static List<UserScore> ReadAllScores(string username)
    {
        string path = string.Format("{0}/{1}.txt",
                                    Application.persistentDataPath,
                                    username);

        if (File.Exists(path))
        {
            List<UserScore> userScores = new List<UserScore>();
            string[] perguntas = File.ReadAllLines(path);

            try
            {
                for (int i = 0; i < perguntas.Length; i++)
                {
                    string[] conteudoScore = perguntas[i].Split(new char[] { '|' }, System.StringSplitOptions.RemoveEmptyEntries);
                    if (conteudoScore.Length >= 4)
                    {
                        UserScore question = new UserScore(username)
                        {
                            AnimalId = (EAnimal)int.Parse(conteudoScore[0]),
                            QuestionId = int.Parse(conteudoScore[1]),
                            CorrectAnswers = int.Parse(conteudoScore[2]),
                            WrongAnswers = int.Parse(conteudoScore[3]),
                        };

                        userScores.Add(question);
                    }
                    else
                    {
                        Debug.Log("Pulado linha em branco no arquivo " + path);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log("Erro ao tentar ler scores: " + ex.Message);

                throw;
            }

            return userScores;
        }
        else
        {
            Debug.Log(string.Format("Arquivo '{0}' não existe.", path));

            return null;
        }
    }

    public static string GetUserFilePath(string username)
    {
        return string.Format("{0}/{1}.txt",
                        Application.persistentDataPath,
                        username);
    }

    /// <summary>
    /// CAREFUL!!!!!!! All current saved scores will be erased!
    /// </summary>
    public static void ResetAllUsersScore()
    {
        var files = Directory.GetFiles(Application.persistentDataPath);
        foreach (var filePath in files)
        {
            var fileNamePath = Path.GetFileNameWithoutExtension(filePath);
            int temp = 0;

            //Delete all files that does not start with a number (animals will start with a number)
            if (!int.TryParse(fileNamePath, out temp))
                File.Delete(filePath);
        }
    }
}
