﻿using UnityEngine;
using Vuforia;

public class AnimalTargetController : MonoBehaviour, ITrackableEventHandler
{
    public int animalId;
    public AudioClip animalSound;
    public AudioClip[] animalSounds;

    private GameWorldController gameController;
    private TrackableBehaviour mTrackableBehaviour;
    private Animator targetAnimator;

    void Start()
    {
        targetAnimator = GetComponent<Animator>();
        gameController = GameObject.Find("GameController").GetComponent<GameWorldController>();
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }

    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            // Play audio when target is found
            //audio.Play();
            Debug.Log("Detectei o alvo " + animalId);
            if (gameController != null)
            {
                gameController.AnimalCardDetected((Assets.Scripts.App_Code.EAnimal)animalId);                

                if (animalSound != null)
                {
                    if (gameController.mainAudioSource.isPlaying)
                        gameController.mainAudioSource.Stop();

                    gameController.mainAudioSource.clip = animalSound;
                    gameController.mainAudioSource.Play();
                }

                if (targetAnimator != null)
                {
                    targetAnimator.Play("DetectedAnimation");
                }
            }

            //Precisa colocar uma thread para rodar cada som e esperar terminá-los
            //foreach (var sound in animalSounds)
            //    AudioSource.PlayClipAtPoint(sound, this.transform.position, 1);
        }
        else
        {
            // Stop audio when target is lost            
            Debug.Log("Perdi o alvo" + animalId);

            if (gameController != null)
            {
                gameController.AnimalCardLost((Assets.Scripts.App_Code.EAnimal)animalId);

                //Stop current audio, if any
                if (animalSound != null)
                {
                    if (gameController.mainAudioSource.isPlaying)
                        gameController.mainAudioSource.Stop();
                }
            }

            if (targetAnimator != null)
                targetAnimator.Play("LostAnimation");
        }
    }
}