﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts.App_Code;
using System.Collections.Generic;
using Assets.Scripts.Util;
using UnityEngine.SceneManagement;

public class GameWorldController : MonoBehaviour
{
    #region Public

    public GameObject allAnswersPanel;
    public GameObject questionPanel;
    public GameObject correctAnswerPanel;
    public GameObject wrongAnswerPanel;
    public AudioSource mainAudioSource;

    public Text textUsername;
    public Text textAnimal;
    public Text textPergunta;
    public Text textResp1;
    public Text textResp2;
    public Text textResp3;
    public Text textResp4;
    public Text lostCardText;

    #endregion

    #region Fields

    private EAnimal currentAnimal = EAnimal.None;
    private int currentQuestion = 0;
    private string currentAnswerString = string.Empty;
    private Dictionary<EAnimal, List<AnimalQuestion>> dcAllAnimalsQuestions = new Dictionary<EAnimal, List<AnimalQuestion>>();
    private List<UserScore> currentUserScores = new List<UserScore>();

    //Track of vuforia objects
    private bool isAnimalDetected = false;
    private float lostAnimalDetection = 0;
    private bool hasAnswered = false;

    #endregion

    // Use this for initialization
    void Start()
    {
        dcAllAnimalsQuestions = QuestionsFileHandler.ReadAllQuestions();
        currentUserScores = ScoreFileHandler.ReadAllScores(GlobalData.CurrentUsername);
        if (currentUserScores == null)  //First time user is playing
            currentUserScores = new List<UserScore>();

        textUsername.text = GlobalData.CurrentUsername;

        //Prepare enviroment, which panels should be visible and which ones shouldn't.
        questionPanel.SetActive(false);
        correctAnswerPanel.SetActive(false);
        wrongAnswerPanel.SetActive(false);
        lostCardText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        if (!isAnimalDetected && currentAnimal != EAnimal.None && !hasAnswered)
        {
            lostAnimalDetection += Time.deltaTime;

            if (lostAnimalDetection >= 5)
            {
                LostCardReference();
            }
            else
            {
                lostCardText.text = string.Format("Fique com o tablet apontando para a carta.\r\nA pergnta irá sumir em: {0} segundo(s)",
                    Mathf.CeilToInt(lostAnimalDetection *-1 + 5));
                
            }
        }
    }

    private void CorrectAnswer()
    {
        questionPanel.SetActive(false);
        correctAnswerPanel.SetActive(true);

        //--Save userscore
        bool foundAnimal = false;
        bool foundQuestion = false;
        foreach (var item in currentUserScores)
        {
            if (item.AnimalId == currentAnimal)
                foundAnimal = true;
            if (item.QuestionId == currentQuestion)
                foundQuestion = true;

            if (foundAnimal && foundQuestion)
            {
                item.CorrectAnswers++;
                break;
            }

            //Reset for next iteration.
            foundAnimal = false;
            foundQuestion = false;
        }

        //Let's add a new score for this animal & question
        if (!foundAnimal && !foundQuestion)
            currentUserScores.Add(new UserScore(GlobalData.CurrentUsername) { AnimalId = currentAnimal, QuestionId = currentQuestion, CorrectAnswers = 1, WrongAnswers = 0 });

        ScoreFileHandler.WriteAllScores(GlobalData.CurrentUsername, currentUserScores);

        //Descarte esta carta, e vire outra carta
    }

    private void WrongAnswer()
    {
        questionPanel.SetActive(false);
        wrongAnswerPanel.SetActive(true);

        //--Save userscore
        bool foundAnimal = false;
        bool foundQuestion = false;
        foreach (var item in currentUserScores)
        {
            if (item.AnimalId == currentAnimal)
                foundAnimal = true;
            if (item.QuestionId == currentQuestion)
                foundQuestion = true;

            if (foundAnimal && foundQuestion)
            {
                item.WrongAnswers++;
                break;
            }

            //Reset for next iteration.
            foundAnimal = false;
            foundQuestion = false;
        }

        //Let's add a new score for this animal & question
        if (!foundAnimal && !foundQuestion)
            currentUserScores.Add(new UserScore(GlobalData.CurrentUsername) { AnimalId = currentAnimal, QuestionId = currentQuestion, CorrectAnswers = 0, WrongAnswers = 1 });

        ScoreFileHandler.WriteAllScores(GlobalData.CurrentUsername, currentUserScores);
    }
    

    private void TimedOut()
    {

    }

    private void LostCardReference()
    {
        currentAnimal = EAnimal.None;
        questionPanel.SetActive(false);
        lostCardText.gameObject.SetActive(false);
    }

    #region Public Methods

    public void RespostaSelected(Button sender)
    {
        //If lost the card, but the user has answered, then let's just erase the text message
        lostCardText.gameObject.SetActive(false);

        //user has answered, let's flag it so if the user remove the card, the LostCard popup won't be displayed!
        hasAnswered = true;

        if (sender != null)
        {
            string buttonText = sender.GetComponentInChildren<Text>().text;
            Debug.Log(string.Format("Selecionado botão {0} com a resposta {1}", sender.name, buttonText));

            if (buttonText == currentAnswerString)
            {
                CorrectAnswer();
            }
            else
            {
                WrongAnswer();
            }
        }
        else
        {
            throw new System.Exception("O evento do botão não foi corretamente configurado");
        }
    }
    
    public void CorrectAnswerPanel_OkButton_OnClick()
    {
        correctAnswerPanel.SetActive(false);
    }

    public void WrongAnswerPanel_OkButton_OnClick()
    {
        wrongAnswerPanel.SetActive(false);

    }

    public void GoTo_MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void AnimalCardDetected(EAnimal animal)
    {
        lostCardText.gameObject.SetActive(false);
        isAnimalDetected = true;

        //Check if it is a new animal... 
        if (animal != currentAnimal)
        {
            hasAnswered = false;
            questionPanel.SetActive(true);
            currentAnimal = animal;

            if (dcAllAnimalsQuestions.ContainsKey(animal))
            {
                allAnswersPanel.SetActive(true);

                var questions = dcAllAnimalsQuestions[animal];
                var selectedQuestion = questions[Random.Range(0, questions.Count - 1)];

                //Save questionid and answer
                currentQuestion = selectedQuestion.QuestionId;
                currentAnswerString = selectedQuestion.Answer1;

                //Set the UI text for the animal name and question text
                textAnimal.text = selectedQuestion.AnimalName;
                textPergunta.text = selectedQuestion.QuestionText;

                //Shuffle all the possible answers
                var answers = new List<string>()
                {
                    selectedQuestion.Answer1, selectedQuestion.Answer2, selectedQuestion.Answer3, selectedQuestion.Answer4
                };
                answers.Shuffle();

                textResp1.text = answers[0];
                textResp2.text = answers[1];
                textResp3.text = answers[2];
                textResp4.text = answers[3];
            }
            else
            {
                textAnimal.text = AnimalQuestion.GetAnimalNameFromId(animal);
                textPergunta.text = "ERRO: O animal escolhido não possui perguntas cadastradas.";
                allAnswersPanel.SetActive(false);
            }
        }
    }

    public void AnimalCardLost(EAnimal animal)
    {
        isAnimalDetected = false;
        lostAnimalDetection = 0;
        
        if (questionPanel != null)
        {
            //Se perdeu o animal e não respondeu a pergunta.... informa o texto... e retorna a pergunta
            if (!hasAnswered)
            {
                lostCardText.gameObject.SetActive(true);
            }
            //Caso contrário deixa como está... para o usuário interagir com o animal...

            //Talvez não deva tirar?
            //questionPanel.SetActive(false);
        }
    }

    #endregion
}
